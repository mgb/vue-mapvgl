# 安装

---

## npm 安装

推荐 npm 安装。

```
npm install vue-bmap-gl --save
npm install vue-mapvgl --save
```

## CDN

```html
<script src="//unpkg.com/vue-bmap-gl/dist/index.js" />
<script src="//unpkg.com/vue-bmap-gl/dist/style.css" />
<script src="//unpkg.com/vue-mapvgl/dist/index.js" />
```
